package aplicacao;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

import entidades.Funcionario;
import entidades.FuncionarioTercerizado;

public class Programa {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		List<Funcionario> list = new ArrayList<>();
		
		System.out.print("Quantidade de funcionarios: ");
		int n = sc.nextInt();
		
		for (int i = 1; i <= n; i++) {
			System.out.println();
			System.out.println("Dados do Funcionario #" + i);
			System.out.print("Tercerizado (s/n)? ");
			char t = sc.next().charAt(0);
			sc.nextLine();
			System.out.print("Nome: ");
			String nome = sc.nextLine();
			System.out.print("Horas: ");
			int horas = sc.nextInt();
			System.out.print("Valor por Hora: ");
			double valorPorHora = sc.nextDouble();
			
			if (t == 's') {
				System.out.print("Despesa adicional: ");
				double despesaAdicional = sc.nextDouble();
				
				list.add(new FuncionarioTercerizado(nome, horas, valorPorHora, despesaAdicional));
			}else {
				list.add(new Funcionario(nome, horas, valorPorHora));
			}			
		}
		
		System.out.println();
		
		System.out.println("Pagamentos");
		
		for (Funcionario func : list) {
			System.out.println(func.getNome() + " - " + String.format("%.2f", func.pagamento()));
		}
		
		
		sc.close();

	}

}
